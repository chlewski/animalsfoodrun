const express = require('express');
const router = express.Router();
const SingleRound = require('./../models/SingleRound');
const Player = require('./../models/Player');
const Pet = require('./../models/Pet');
const jwt = require('jsonwebtoken');
const verifyToken = require('../verifyToken');

//  Add new round to game
router.post('/add-round', (req, res) => {
    let round = new SingleRound(req.body);
    round.save((error, savedRound) => {

        if (error) {
            res.status(500).send(error);
            console.log(error);
        } else {
            res.status(200).send(savedRound);
            console.log(savedRound);
        }
    });
});

//  Get all existing rounds
router.get('/get-all-rounds', verifyToken, (req, res) => {
    SingleRound.find((err, rounds) => {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        } else {
            res.status(200).send(rounds);
        }
    });
});

//  Get single round by id
router.get('/get-single-round/:id', verifyToken, (req, res) => {
    SingleRound.findOne({ _id: req.params.id }, (err, round) => {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        } else {
            res.status(200).send(round);
        }
    });
});

//  Get player details
router.get('/get-player-details/', verifyToken, (req, res) => {
    Player.findOne({ userId: req.userId }, (err, player) => {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        } else {
            res.status(200).send(player);
        }
    });
});

//  Get player details
router.get('/get-pets', verifyToken, (req, res) => {
    Pet.find((err, pet) => {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        } else {
            pet.sort((a, b) => a.cost - b.cost);
            res.status(200).send(pet);
        }
    });
});

//  Add new pet to game
router.post('/add-pet', (req, res) => {
    let pet = new Pet(req.body);
    pet.save((error, savedPet) => {

        if (error) {
            res.status(500).send(error);
            console.log(error);
        } else {
            res.status(200).send(savedPet);
            console.log(savedPet);
        }
    });
});

//  Add pet to user 
router.post('/add-pet-user', verifyToken, (req, res) => {
    Player.findOne({ userId: req.userId }, (err, player) => {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        } else {
            player.petsArray.push({ petId: req.body.petId, name: req.body.petName });

            petsArray = player.petsArray;
            score = player.score - req.body.petCost;

            Player.updateOne({ userId: req.userId }, {
                    $set: {
                        petsArray,
                        score
                    }
                },
                (err, data) => {
                    if (err) {
                        console.log(err);
                        res.status(500).send(err);
                    } else {
                        res.status(200).send(data);
                    }
                }
            );
        }
    })
})

//  set pet to user 
router.post('/set-pet-user', verifyToken, (req, res) => {
    Player.findOne({ userId: req.userId }, (err, player) => {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        } else {
            petType = req.body.petName;
            Player.updateOne({ userId: req.userId }, {
                    $set: {
                        petType
                    }
                },
                (err, data) => {
                    if (err) {
                        console.log(err);
                        res.status(500).send(err);
                    } else {
                        res.status(200).send(data);
                    }
                }
            );
        }
    })
})

//  score update for player
router.post('/score-update', verifyToken, (req, res) => {
    Player.findOne({ userId: req.userId }, (err, player) => {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        } else {

            player.score += req.body.score;
            let roundsFinishedCopy = JSON.parse(JSON.stringify(player.roundsFinished));

            let isCopy = false;
            let status = '';

            for (let i = 0; i < roundsFinishedCopy.length; i++) {
                if (roundsFinishedCopy[i].roundId === req.body.roundId) {
                    if (roundsFinishedCopy[i].stars >= req.body.stars && roundsFinishedCopy[i].score >= req.body.score) {
                        isCopy = true;
                        status = 'WITHOUT_UPDATE';
                        break;
                    }
                    roundsFinishedCopy[i].stars = req.body.stars;
                    roundsFinishedCopy[i].score = req.body.score;
                    isCopy = true;
                    console.log('with update')
                    status = 'WITH_UPDATE';
                    break;
                }
            }

            if (!isCopy) {
                status = 'NEW_RECORD';
                roundsFinishedCopy.push({ roundId: req.body.roundId, stars: req.body.stars, score: req.body.score })
            }


            roundsFinished = roundsFinishedCopy;
            score = player.score;
            let dataSend = {}

            if (status === 'WITHOUT_UPDATE') {
                dataSend = {
                    message: 'You already have the score and is better than the current one.',
                    stars: req.body.stars
                }
            }
            if (status === 'WITH_UPDATE' || status === 'NEW_SCORE') {
                dataSend = {
                    message: 'Congratulations this is your best result',
                    stars: req.body.stars
                }
            }

            Player.updateOne({ userId: req.userId }, {
                    $set: {
                        roundsFinished,
                        score
                    }
                },
                (err, data) => {
                    if (err) {
                        console.log(err);
                        res.status(500).send(err);
                    } else {
                        res.status(200).send(data);
                    }
                }
            );
        }
    });

});

//  update user bonus
router.post('/bonus-update', verifyToken, (req, res) => {
    Player.findOne({ userId: req.userId }, (err, player) => {
        player.bonusesArray.forEach(element => {
            if (element.bonusName === req.body.bonusName)
                element.bonusLevel = req.body.bonusLevel;
        });

        bonusesArray = player.bonusesArray;
        score = player.score - req.body.bonusCost;

        Player.updateOne({ userId: req.userId }, {
            $set: {
                bonusesArray,
                score
            }
        }, (err, data) => {
            if (err) {
                console.log(err);
                res.status(500).send(err);
            } else {
                res.status(200).send(data);
            }
        })

    });
});

//  add score after finish round
router.post('/add-score', verifyToken, (req, res) => {
    SingleRound.findOne({ _id: req.body.roundId }, (err, round) => {
        if (err) {
            res.status(500).send(err);
        } else {
            Player.findOne({ userId: req.userId }, (err, player) => {
                let isCopy = false;
                let bestScores = round.bestScores;
                bestScores.forEach(el => {
                    if (el.userName === player.userName && el.score >= req.body.score) {
                        isCopy = true;
                        console.log('nie tutaj')
                    }
                    if (el.userName === player.userName && el.score < req.body.score) {
                        el.score = req.body.score;
                        console.log('tutuaj')
                        isCopy = true;
                        return;
                    }
                })
                if (!isCopy) {
                    console.log('push')
                    bestScores.push({ userName: player.userName, score: req.body.score });
                }
                SingleRound.updateOne({ _id: req.body.roundId }, {
                        $set: {
                            bestScores
                        }
                    },
                    (err, data) => {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            res.status(200).send(data);
                        }
                    })
            });

        }
    })
});

//  get best scores 
router.get('/get-high-scores/:id', verifyToken, (req, res) => {
    SingleRound.findById({ _id: req.params.id }, (err, round) => {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            round.bestScores.sort((a, b) => { return b.score - a.score });
            Player.findOne({ userId: req.userId }, (err, player) => {
                if (err) {
                    console.log(err);
                    res.status(500).send(err);
                } else {
                    round.bestScores.forEach(el => {
                        if (el.userName === player.userName) {
                            el.myScore = true;
                        }
                    });
                    res.status(200).send(round.bestScores);
                }
            })
        }
    })
})


module.exports = router;