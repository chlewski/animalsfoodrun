const express = require('express');
const router = express.Router();
const User = require('./../models/User');
const Player = require('./../models/Player');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const SECRET_KEY = 'KEY'

//  sign up 
router.post('/signup', (req, res) => {
    let user = new User(req.body);

    User.findOne({ username: user.username }, (err, userFound) => {
        if (userFound || err) {
            console.log("User exists");
            res.status(400).send("User exists");
        } else {
            bcrypt.genSalt(saltRounds, (err, salt) => {
                if (err) {
                    res.status(500).send('Internal error');
                } else {

                    bcrypt.hash(user.password, salt, (err, hash) => {
                        if (err) {
                            res.status(500).send('Internal error');
                        } else {
                            user.password = hash;
                            user.save((err, savedUser) => {
                                if (err) {
                                    console.log(err);
                                    res.status(500).send('Internal error');
                                } else {
                                    let petsArray = new Array();
                                    petsArray.push({ petId: "5ddff247f2ad4f1748207e90", name: "pig" });
                                    let bonusesArray = new Array();
                                    bonusesArray.push({ bonusName: 'Health', bonusLevel: 1 }, { bonusName: 'Coin', bonusLevel: 1 }, { bonusName: 'Gravity', bonusLevel: 1 });
                                    let player = new Player({
                                        userId: savedUser._id,
                                        roundsFinished: {},
                                        petType: 'pig',
                                        score: 0,
                                        petsArray: petsArray,
                                        bonusesArray: bonusesArray,
                                        userName: user.username
                                    });
                                    player.save();
                                    const payload = {
                                        subject: user._id
                                    };
                                    const token = jwt.sign(payload, SECRET_KEY);

                                    res.status(200).send({
                                        token
                                    });
                                }
                            })
                        }

                    })
                }
            });
        }
    })

});


//  Sign in 
router.post('/signin', (req, res) => {

    User.findOne({ username: req.body.username }, (error, user) => {

        if (error) {
            console.log(error);
            res.status(500).send(error);
        } else {
            if (!user) {
                res.status(400).send("Incorrect login or password");
            } else {
                bcrypt.compare(req.body.password, user.password, (error, hash) => {
                    if (error) {
                        res.status(500).send('Internal error');
                    } else {
                        console.log('Hash:', hash)
                        if (!hash) {
                            res.status(400).send("Incorrect login or password");
                        } else {
                            const payload = {
                                subject: user._id
                            };
                            const token = jwt.sign(payload, SECRET_KEY);
                            res.status(200).send({
                                token
                            });
                        }
                    }
                });
            }
        }

    })
});

module.exports = router;