//Requirement Libraries
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);


const PORT = 3000;
const DB_URL = 'mongodb+srv://root:root@cluster0-eatcx.mongodb.net/test?retryWrites=true&w=majority';
const IP_ADDRESS = 'localhost';
//const IP_ADDRESS = '192.168.43.60';
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Import Routes
const authRoute = require('./routes/auth');
const gameRoute = require('./routes/game');

//Middlewares
app.use('/auth', authRoute);
app.use('/game', gameRoute);

//Basic end-point to check if server work
app.get('/', (req, res) => {
    res.status(200).send('SERVER IS OK!');
});

//Connect to database
mongoose.connect(DB_URL, { useNewUrlParser: true },
    (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log('connected to mongodb');
        }
    });

//Server listener
app.listen(PORT, IP_ADDRESS, () => {
    console.log(`app listen on port : ${PORT}, IP: ${IP_ADDRESS} `);
});