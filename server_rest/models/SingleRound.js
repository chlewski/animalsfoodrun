//Single Round model
const mongoose = require('mongoose');

const SingleRoundSchema = mongoose.Schema({
    roundNumber: Number,
    background: String,
    maxScore: Number,
    obstaclesArray: Array,
    bestScores: Array,
});

module.exports = mongoose.model('SingleRound', SingleRoundSchema, 'singleRound');