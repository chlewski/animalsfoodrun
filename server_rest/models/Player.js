//Player model
const mongoose = require('mongoose');

const PlayerSchema = mongoose.Schema({
    userId: String,
    roundsFinished: Array,
    bonusesArray: Array,
    petsArray: Array,
    petType: String,
    score: Number,
    stars: Number,
    userName: String
});

module.exports = mongoose.model('Player', PlayerSchema, 'player');