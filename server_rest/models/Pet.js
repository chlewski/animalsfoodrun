//Pet model
const mongoose = require('mongoose');

const PetSchema = mongoose.Schema({
    name: String,
    cost: Number
});

module.exports = mongoose.model('Pet', PetSchema, 'pet');