import { enableProdMode } from '@angular/core';


import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { platformBrowser } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Media } from '@ionic-native/media/ngx';
import 'hammerjs';

if (environment.production) {
  enableProdMode();
}
const bootstrap = () => {
  platformBrowserDynamic().bootstrapModule(AppModule);
};

if (typeof window['cordova'] !== 'undefined') {
  document.addEventListener('deviceready', () => {
    bootstrap();
    console.log(Media);
  }, false);
} else {
  bootstrap();
}


