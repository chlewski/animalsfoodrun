import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { ChangeImage } from 'src/app/use-classes/ChangeImage';
import { NestedService } from 'src/app/services/nested.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  user: User = new User();
  errorMessage: string | null;
  registerForm: FormGroup;
  backgroundOnTop: string = 'assets/backgrounds/background0.jpg';
  changeImageController = new ChangeImage();
  interval: any;
  constructor(private nestedService: NestedService, private router: Router) { }

  ngOnInit() {
    this.formValidation();
    this.backgroundShow();
  }

  //Change background
  backgroundShow(): void {
    let i = 0;
    this.interval = setInterval(() => {
      if (i === this.changeImageController.actualImage.length) {
        i = 1;
      }
      this.backgroundOnTop = this.changeImageController.setImage(i++);
    }, 3000)
  }
  // Method to validate form
  formValidation(): void {
    this.registerForm = new FormGroup({
      username: new FormControl(this.user.username, [
        Validators.required
      ]),
      password: new FormControl(this.user.password, [
        Validators.required
      ])
    });
  }

  // Method to send data to REST
  onSubmit(): void {
    this.nestedService.authService.singUp(this.user.username, this.user.password)
      .subscribe(
        res => localStorage.setItem('token', res.token),
        err => this.errorMessage = err.error,
        () => this.router.navigate(['/game'])
      );
  }
  ngOnDestroy(): void {
    clearInterval(this.interval);
  }
}
