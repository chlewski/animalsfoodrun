import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/user';
import { NestedService } from 'src/app/services/nested.service';
import { RouterLink, Router } from '@angular/router';
import { ChangeImage } from 'src/app/use-classes/ChangeImage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  user: User = new User();
  errorMessage: string | null;
  backgroundOnTop: string = 'assets/backgrounds/background0.jpg';
  changeImageController = new ChangeImage();
  interval: any;
  constructor(private nestedService: NestedService, private router: Router) {
  }

  ngOnInit() {
    this.formValidation();
    this.backgroundShow();
  }

  //Change background
  backgroundShow() {
    let i = 0;
    this.interval = setInterval(() => {
      if (i === this.changeImageController.actualImage.length) {
        i = 1;
      }
      this.backgroundOnTop = this.changeImageController.setImage(i++);
      console.log('0interval')
    }, 3000)
  }

  // Method to validate form
  formValidation() {
    this.loginForm = new FormGroup({
      username: new FormControl(this.user.username, [
        Validators.required
      ]),
      password: new FormControl(this.user.password, [
        Validators.required
      ])
    });
  }

  // Method to send data to REST
  onSubmit(): void {
    this.nestedService.authService.logIn(this.user.username, this.user.password)
      .subscribe(
        res => localStorage.setItem('token', res.token),
        err => console.log('HTTP Error', this.errorMessage = err.error),
        () => this.router.navigate(['/game'])
      );
  }
  ngOnDestroy(): void {
    clearInterval(this.interval);
  }
}
