import { GameTypes } from './gameTypes';

export class CollisionType {
    type: string;
    gameTypes: GameTypes;
    index: number;
}