export class Obstacle {
    posX: number;
    posY: number;
    obstacleWidth: number;
    obstacleHeight: number;
    background: any;
    type: string;
    context: any;
    canvasWidth: any;
    canvasHeight: any;
    eat: string;

    constructor(posX, posY, obstacleHeight, type, canvasWidth, canvasHeight, context, eat) {

        this.context = context;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.type = type;
        this.posX = this.canvasWidth + posX;
        this.background = new Image();

        if (this.type === 'pipe') {
            this.posY = this.canvasHeight - posY / 100 * this.canvasHeight;
            this.obstacleWidth = 50;
            this.obstacleHeight = obstacleHeight / 100 * this.canvasHeight;
            this.background.src = 'assets/pipe_bottom.png';
        }

        if (this.type === 'pipe_top') {
            this.posY = 0;
            this.obstacleWidth = 50;
            this.obstacleHeight = obstacleHeight / 100 * this.canvasHeight;
            this.background.src = 'assets/pipe_top.png';
        }

        if (this.type === 'score') {
            this.posY = canvasHeight - posY / 100 * this.canvasHeight;
            this.obstacleWidth = 50;
            this.obstacleHeight = 50;
            this.eat = eat;
            this.background.src = `assets/eat/${this.eat}.png`;
            console.log(this.background.src, 'xd')
        }

        if (this.type === 'finish') {
            this.posY = 0;
            this.obstacleWidth = 90;
            this.obstacleHeight = obstacleHeight + this.canvasHeight;
            this.background.src = 'assets/finish.png';
        }
    }

    //draw obstacle with parameters
    drawObstacle(): void {
        this.context.drawImage(this.background, this.posX, this.posY, this.obstacleWidth, this.obstacleHeight);
    }

    //moving obstacle
    moveObstacle(): void {
        this.posX -= 2;
    }
}