export class ScrollingBackground {
    x: number;
    image: string;
    ctx: any;
    canvasWidth: any;
    canvasHeight: any;

    constructor(image: string, ctx: any, canvasWidth: any, canvasHeight: any) {
        this.image = image;
        this.x = 0;
        this.ctx = ctx;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
    }

    updateState() {
        this.x--;  
        
        this.x--;
        if (this.x <= -this.canvasWidth)
        {
            this.x = 0;
        }
    }

    render() {
        this.ctx.drawImage(this.image, this.x, 0, this.canvasWidth, this.canvasHeight);
        this.ctx.drawImage(this.image, this.x + this.canvasWidth, 0, this.canvasWidth, this.canvasHeight);
    }
}