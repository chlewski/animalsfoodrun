import { GameTypes } from './gameTypes';

export class GameButtons {
    textMessage: string;
    buttonsType: GameTypes;
    image1: any;
    image2: any;
    image3: any;
    score: number;
    roundNumber: number;
    roundId: string;
    maxScore: number;
    stars: number;
    hearts: number;
    coinsMultiplier: number;
}