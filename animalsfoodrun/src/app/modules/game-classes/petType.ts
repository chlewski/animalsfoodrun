export class PetType {
    _id: string;
    name: string;
    cost: number;
}