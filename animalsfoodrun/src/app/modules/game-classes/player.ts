import { GameTypes } from './gameTypes';
import { CollisionType } from './CollisionType';

export class Player {
    posX: number;
    posY: number;
    canvasWidth: any;
    canvasHeight: any;
    context: any;
    gravity: number;
    background: any;
    bonusesArray: any;
    dy: number;


    constructor(posX, posY, gravity, canvasWidth, canvasHeight, context, backgroundSrc, bonusesArray) {
        this.posX = posX;
        this.posY = canvasHeight / 2;
        this.gravity = gravity;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.context = context;
        this.dy = 40;
        this.bonusesArray = bonusesArray        
        // init image;
        this.background = new Image();
        this.background.src = backgroundSrc;
    }
    firstDraw(): void {
        this.background.onload = () => {
            this.context.drawImage(this.background, this.posX - 25, this.posY - 25, 50, 50);
        }
    }
    draw(): void {
        this.context.drawImage(this.background, this.posX - 25, this.posY - 25, 50, 50);
    }

    move(): void {
        this.posY += this.gravity;
    }

    collisions(obstacle, index): CollisionType {
        let collisionType = new CollisionType();
        if (this.posY - 25 <= 0) {
            collisionType.gameTypes = GameTypes.END_GAME;
            collisionType.type = 'canvas_top';
            return collisionType;
        }

        if (this.posY + 25 >= this.canvasHeight) {
            collisionType.gameTypes = GameTypes.END_GAME;
            collisionType.type = 'canvas_bottom';
            return collisionType;
        }

        if (this.posX + 25 >= obstacle.posX && this.posX + 25 <= obstacle.posX + 25 + (obstacle.obstacleWidth)) {

            if (this.posY + 25 >= obstacle.posY && this.posY + 25 <= obstacle.posY + obstacle.obstacleHeight) {

                if (obstacle.type === 'pipe') {
                    collisionType.gameTypes = GameTypes.END_GAME;
                    collisionType.index = index;
                    collisionType.type = 'pipe_bottom'
                }

                if (obstacle.type === 'score') {
                    collisionType.type = 'score';
                    collisionType.gameTypes = GameTypes.SCORE;
                    collisionType.index = index;
                }

                if (obstacle.type === 'finish') {
                    collisionType.type = 'finish';
                    collisionType.gameTypes = GameTypes.WIN;
                    collisionType.index = index;
                }
            }
            if (this.posY >= obstacle.obstacleHeight && this.posY <= obstacle.posY) {
                if (obstacle.type === 'score') {
                    collisionType.type = 'score';
                    collisionType.gameTypes = GameTypes.SCORE;
                    collisionType.index = index;
                }
            }
            if (this.posY - 25 <= obstacle.obstacleHeight) {

                if (obstacle.type === 'finish') {
                    collisionType.type = 'finish';
                    collisionType.gameTypes = GameTypes.WIN;
                    collisionType.index = index;
                }

                if (obstacle.type === 'pipe_top') {
                    collisionType.type = 'pipe_top';
                    collisionType.gameTypes = GameTypes.END_GAME;
                    collisionType.index = index;
                }
            }
        } else {
            collisionType.gameTypes = GameTypes.NONE;
        }

        if (this.posX === obstacle.posX + obstacle.obstacleWidth / 2 || this.posX === obstacle.posX) {
            if (obstacle.type === 'pipe' || obstacle.type === 'pipe_top') {
                collisionType.type = 'score';
                collisionType.gameTypes = GameTypes.SCORE;
                collisionType.index = index;
            }
        }
        return collisionType;
    }

    action(): void {
        /* for(let i = this.posY; i<this.posY-this.dy; i = i+0.1){
             
        / }*/
        this.posY -= this.dy;
        console.log(this.posY);
    }
}
