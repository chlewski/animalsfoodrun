export enum GameTypes{
    END_GAME,
    END_GAME_WITH_CHANCE,
    WIN,
    SCORE,
    NONE
}