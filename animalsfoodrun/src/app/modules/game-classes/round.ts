export class Round{
    _id: string;
    roundNumber: number;
    background: string;
    obstaclesArray: Array<any>;
    maxScore: number;
}