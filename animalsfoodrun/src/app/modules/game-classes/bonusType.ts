export class BounsType {
    bonusName: string;
    bonusLevel: number;
    bonusDescription: string;  
    cost: number;  
}
