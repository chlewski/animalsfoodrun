import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Obstacle } from '../../game-classes/obstacle';
import { Player } from '../../game-classes/player';
import { GameTypes } from '../../game-classes/gameTypes';
import { GameButtons } from '../../game-classes/gameButtons';
import { Router, ActivatedRoute } from '@angular/router';
import { NestedService } from 'src/app/services/nested.service';
import { Round } from '../../game-classes/round';
import { CollisionType } from '../../game-classes/CollisionType';
import { ScrollingBackground } from '../../game-classes/ScrollingBackground';

@Component({
    selector: 'app-game',
    templateUrl: './game-logic.component.html',
    styleUrls: ['./game-logic.component.scss']
})
export class GameLogicComponent implements OnInit, OnDestroy {
    vibrationInterval: any;
    resurectionInterval: any;
    pauseInterval: any;
    ngOnDestroy(): void {
        window.clearInterval(this.gameInterval);
        window.clearInterval(this.vibrationInterval);
        window.clearInterval(this.resurectionInterval);
        window.clearInterval(this.pauseInterval);
    }

    @ViewChild('canvas', { static: true }) canvas;
    scrHeight: any;
    scrWidth: any;
    eat: string;

    @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
        this.canvas.nativeElement.width = window.innerWidth;
        this.canvas.nativeElement.height = window.innerHeight;
    }


    time: number = 0;
    progress: number = null;
    private ctx: CanvasRenderingContext2D;
    player: Player;
    obstacles: Array<Obstacle> = [];
    gameInterval: any;
    score: number = 0;
    gameButtonsOpen: boolean;
    buttons: GameButtons;
    tapScreen: boolean;
    background: any;
    toggleGameButton: boolean;
    menuButton: boolean;
    showMainButtons: boolean;
    currentRoundId: string;
    roundData: Round;
    actualRoundNumber: number;
    roundMaxScore: number;
    playerBonuses: Array<any> = new Array();
    buttonsReq: {};
    collisionType: CollisionType;
    extraLives: number;
    coinMultiplier: number;
    gravity: number;
    scrollingBackground: ScrollingBackground;

    constructor(private router: ActivatedRoute, private nestedSerivce: NestedService, private routerNav: Router) { }

    ngOnInit(): void {
        this.getScreenSize();
        // async functions 
        this.getIdFromRoute();
        this.getRoundData();
        console.log(this.eat)
        console.log(window.innerWidth, this.canvas.nativeElement.width);
    }

    // getRoundData
    getRoundData(): void {
        this.nestedSerivce.gameService.getSingleRound(this.currentRoundId)
            .subscribe(
                res => {
                    console.log(res);
                    this.roundData = res;
                    this.actualRoundNumber = res.roundNumber;
                    this.roundMaxScore = res.maxScore;
                    this.ctx = this.canvas.nativeElement.getContext('2d');
                    this.getScreenSize();
                    this.firstLook();
                    this.startGame = this.startGame.bind(this);
                    this.playerActions = this.playerActions.bind(this);
                    this.canvas.nativeElement.addEventListener('click', this.startGame, false);
                },
                err => console.log(err)
            );
    }

    //  get current round id from router path
    getIdFromRoute(): void {
        this.router.params.subscribe(params => {
            this.currentRoundId = params['id'];
            console.log("currentId: ", this.currentRoundId);
        });
    }

    //  first look
    firstLook(): void {
        this.background = new Image();

        this.background.src = `assets/backgrounds/${this.roundData.background}`;
        this.background.onload = () => {
            this.ctx = this.canvas.nativeElement.getContext('2d');
            this.scrollingBackground = new ScrollingBackground(this.background, this.ctx, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
            this.initCanvas();
            this.initPlayer();
        }
    }
    //  game loop
    gameLoop(): void {
        this.showMainButtons = true;
        this.toggleGameButton = true;
        this.canvas.nativeElement.addEventListener('click', this.playerActions, false);
        let coinRotate = 0;
        window.requestAnimationFrame(() => {
            this.gameInterval = window.setInterval(() => {
                this.canvasReload();
                this.obstacles.forEach((el, index) => {
                    el.drawObstacle();
                    el.moveObstacle();
                    this.checkCollisions(this.player.collisions(el, index), index);
                });
                this.player.move();
                this.player.draw();
                this.drawScore();
            }, 1000 / 60);
        });
    }

    //  initialize player
    initPlayer(): void {
        this.nestedSerivce.gameService.getPlayerAssets().subscribe(data => {
            this.playerBonuses = data.bonusesArray;
            this.extraLives = this.playerBonuses[0].bonusLevel - 1;
            this.coinMultiplier = this.playerBonuses[1].bonusLevel;
            this.gravity = this.playerBonuses[2].bonusLevel;
            this.gravity === 1 ? this.gravity = 1.4 : this.gravity === 2 ? this.gravity = 1.3 :
                this.gravity === 3 ? this.gravity = 1.2 : this.gravity === 4 ? this.gravity = 1.1 :
                    this.gravity === 5 ? this.gravity = 1.0 : null;
            this.player = new Player(100,
                this.canvas.nativeElement.height,
                this.gravity,
                this.canvas.nativeElement.width,
                this.canvas.nativeElement.height,
                this.ctx, `assets/pets/${data.petType}.png`,
                this.playerBonuses);
            if (data.petType === 'frog') {
                this.eat = 'fly';
            }
            if (data.petType === 'horse') {
                this.eat = 'carrot';
            }
            if (data.petType === 'pig') {
                this.eat = 'rye';
            }
            this.initObstacles();
            this.player.firstDraw();
        });
    }

    //  initialize canvas width height background
    initCanvas(): void {

        this.scrollingBackground.render();
        this.scrollingBackground.updateState();
        //console.log('init canvas')
        //this.ctx.drawImage(this.background, 0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }

    //  read obstacles from rest 
    initObstacles(): void {
        // posX, posY, obstacleWidth, obstacleHeight, backgroundSrc, type, canvas, context
        console.log(this.roundData.obstaclesArray);
        console.log(this.eat)
        this.roundData.obstaclesArray.forEach(el => {
            let obstacle = new Obstacle(
                el.posX,
                el.posY,
                el.obstacleHeight,
                el.type,
                this.canvas.nativeElement.width,
                this.canvas.nativeElement.height,
                this.ctx,
                this.eat
            );
            this.obstacles.push(obstacle);
        });
    }

    // draw score function
    drawScore(): void {
        this.ctx.font = '60px Arial';
        this.ctx.fillStyle = '#FF0000';
        this.ctx.fillText(this.score.toString(), this.canvas.nativeElement.width / 2, 50);
    }

    //  reload canvas function
    canvasReload(): void {
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        this.initCanvas();
    }

    //  Check collisions
    checkCollisions(collisionType: CollisionType, obstacleIndex: number) {
        switch (collisionType.gameTypes) {
            case GameTypes.END_GAME:
                if (this.playerBonuses[0].bonusLevel > 1) {
                    this.endGame();
                    this.collisionType = collisionType;
                    this.gameButtonsOpen = true;
                    window.clearInterval(this.gameInterval);
                    this.buttons = {
                        textMessage: 'You can do it again!',
                        buttonsType: GameTypes.END_GAME_WITH_CHANCE,
                        image1: 'assets/undo-button.png',
                        image2: null,
                        image3: 'assets/bonuses/Health.png',
                        score: this.score,
                        roundNumber: this.actualRoundNumber,
                        roundId: this.currentRoundId,
                        maxScore: this.roundMaxScore,
                        stars: 0,
                        hearts: this.playerBonuses[0].bonusLevel - 1,
                        coinsMultiplier: this.playerBonuses[1].bonusLevel
                    }
                    navigator.vibrate(600);
                } else if (this.playerBonuses[0].bonusLevel === 1) {
                    this.endGame();
                    this.gameButtonsOpen = true;
                    console.log(this.actualRoundNumber)
                    this.buttons = {
                        textMessage: 'You Lose!',
                        buttonsType: GameTypes.END_GAME,
                        image1: 'assets/undo-button.png',
                        image2: null,
                        image3: null,
                        score: this.score,
                        roundNumber: this.actualRoundNumber,
                        roundId: this.currentRoundId,
                        maxScore: this.roundMaxScore,
                        stars: 0,
                        hearts: 0,
                        coinsMultiplier: this.playerBonuses[1].bonusLevel
                    }
                    navigator.vibrate(600);
                }
                break;
            case GameTypes.WIN:
                console.log('collision type', collisionType);
                this.endGame();
                this.gameButtonsOpen = true;
                this.buttons = {
                    textMessage: 'Congratulations! You won!',
                    buttonsType: GameTypes.WIN,
                    image1: 'assets/undo-button.png',
                    image2: 'assets/arrow_right.png',
                    image3: null,
                    score: this.score,
                    roundNumber: this.actualRoundNumber,
                    roundId: this.currentRoundId,
                    maxScore: this.roundMaxScore,
                    stars: 0,
                    hearts: 0,
                    coinsMultiplier: this.playerBonuses[1].bonusLevel
                }
                let timer = 0;
                this.vibrationInterval = setInterval(() => {
                    timer++;
                    if (timer === 3) {
                        timer = 0;
                        clearInterval(this.vibrationInterval);
                    }
                    navigator.vibrate(200);
                }, 400)
                navigator.vibrate(200);
                navigator.vibrate(200);
                navigator.vibrate(200);
                break;
            case GameTypes.SCORE:
                this.score += 1 * this.playerBonuses[1].bonusLevel;
                if (this.obstacles[obstacleIndex].type === 'score')
                    this.obstacles.splice(obstacleIndex, 1);
                break;
        }
    }

    // player action
    playerActions(): void {
        this.player.action();
    }

    //  start game function
    startGame(): void {
        this.tapScreen = true;
        this.gameLoop();
        this.canvas.nativeElement.removeEventListener('click', this.startGame, false);
    }

    //  function to pause or continue game 
    pauseGame(): void {
        this.toggleGameButton = !this.toggleGameButton;
        if (!this.toggleGameButton) {
            this.canvas.nativeElement.removeEventListener('click', this.playerActions, false);
            clearInterval(this.gameInterval);
        } else {
            this.pauseInterval = setInterval(() => {
                window.clearInterval(this.gameInterval);
                this.time++;
                this.progress = 20 * this.time;
                if (this.time === 6) {
                    this.time = 0;
                    this.progress = null;
                    this.gameLoop();
                    clearInterval(this.pauseInterval);
                }
            }, 1000);
        }
    }

    //  function to finish game if user loose, win or pause game
    endGame(): void {
        this.showMainButtons = false;
        this.canvas.nativeElement.removeEventListener('click', this.playerActions, false);
        clearInterval(this.gameInterval);

    }

    goToMenu(): void {
        this.routerNav.navigate(['/game/menu']);
    }

    resurection(): void {
        this.extraLives--;
        this.canvasReload();
        window.clearInterval(this.gameInterval);
        this.obstacles.forEach((el, index) => {
            el.drawObstacle();
            el.moveObstacle();
            this.checkCollisions(this.player.collisions(el, index), index);
        });
        this.positionPlayerAfterResurection();
        this.player.draw();

        this.resurectionInterval = setInterval(() => {

            this.time++;
            this.progress = 20 * this.time;
            if (this.time === 6) {
                this.time = 0;
                this.playerBonuses[0].bonusLevel -= 1;
                this.progress = null;
                this.gameLoop();
                clearInterval(this.resurectionInterval);
            }
        }, 1000)
    }
    extraHearth(event: any) {
        if (event.used) {
            console.log(this.gameButtonsOpen);
            this.resurection();
        }
        this.buttons = null;
        this.gameButtonsOpen = false;
    }

    positionPlayerAfterResurection() {
        if (this.collisionType.type === 'canvas_top')
            this.player.posY = this.canvas.nativeElement.height * 1 / 2;

        if (this.collisionType.type === 'canvas_bottom')
            this.player.posY = this.canvas.nativeElement.height * 1 / 2;

        if (this.collisionType.type === 'pipe_top') {
            let flag = false;
            let obstacleIndexFilter = 0;
            this.obstacles.forEach((el, index) => {
                if (this.obstacles[this.collisionType.index].posX === el.posX && index !== this.collisionType.index && (el.type === 'pipe' || el.type === 'pipe_top')) {
                    flag = true;
                    obstacleIndexFilter = index;
                    return;
                }
            });
            if (flag) {
                let respPosY;
                respPosY = (this.obstacles[this.collisionType.index].obstacleHeight + 25 +
                    this.obstacles[obstacleIndexFilter].posY - 25) * 1 / 2;
                this.player.posY = respPosY;
            } else {
                this.player.posY = (this.canvas.nativeElement.height - this.obstacles[this.collisionType.index].obstacleHeight) * 1 / 2;
            }
        }

        if (this.collisionType.type === 'pipe_bottom') {
            console.log('podbitka')
            let flag = false;
            let obstacleIndexFilter = 0;
            this.obstacles.forEach((el, index) => {
                if (this.obstacles[this.collisionType.index].posX === el.posX && index !== this.collisionType.index && (el.type === 'pipe' || el.type === 'pipe_top')) {
                    flag = true;
                    obstacleIndexFilter = index;
                    return;
                }
            });
            if (flag) {
                let respPosY = (this.obstacles[this.collisionType.index].posY +
                    this.obstacles[obstacleIndexFilter].obstacleHeight) * 1 / 2;
                this.player.posY = respPosY;
            } else {
                this.player.posY = (this.canvas.nativeElement.height - this.obstacles[this.collisionType.index].obstacleHeight) * 1 / 2;
                //this.player.posY = this.obstacles[this.collisionType.index].posY - 50;
            }
        }
    }

    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }


}
