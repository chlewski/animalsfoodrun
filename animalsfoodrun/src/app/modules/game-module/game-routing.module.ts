import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game.component';
import { GameMenuComponent } from './components/game-menu/game-menu.component';
import { GameLogicComponent } from './game-logic/game-logic.component';
import { GetLevelComponent } from './components/get-level/get-level.component';
import { GameShopComponent } from './components/game-shop/game-shop.component';
import { TopScorersComponent } from './components/top-scorers/top-scorers.component';



const routes = [
  { path: '', redirectTo: 'menu', pathMatch: 'full' },
  {
    path: '', component: GameComponent, children: [
      { path: 'menu', component: GameMenuComponent },
    ]

  },
  { path: 'play/:id', component: GameLogicComponent },
  { path: 'choose-round', component: GetLevelComponent },
  { path: 'shop', component: GameShopComponent },
  { path: 'top-scorers', component: TopScorersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }