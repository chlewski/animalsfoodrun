
import { NgModule } from '@angular/core';
import { GameComponent } from './game.component';
import { GameRoutingModule } from './game-routing.module';
import { GameMenuComponent } from './components/game-menu/game-menu.component';
import { GameButtonsComponent } from './components/game-buttons/game-buttons.component';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { GameLogicComponent } from './game-logic/game-logic.component';
import { GetLevelComponent } from './components/get-level/get-level.component';
import { GameShopComponent } from './components/game-shop/game-shop.component';
import { TopScorersComponent } from './components/top-scorers/top-scorers.component';
import { HighScoresViewerComponent } from './components/high-scores-viewer/high-scores-viewer.component';


@NgModule({
  declarations: [
    GameComponent,
    GameMenuComponent,
    GameButtonsComponent,
    GameLogicComponent,
    GetLevelComponent,
    GameShopComponent,
    TopScorersComponent,
    HighScoresViewerComponent

  ],
  imports: [
    GameRoutingModule,
    CommonModule
  ],
  providers: [GameRoutingModule],
  bootstrap: [GameComponent]
})
export class GameModule { }