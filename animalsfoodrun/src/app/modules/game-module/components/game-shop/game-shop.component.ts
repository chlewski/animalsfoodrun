import { Component, OnInit, ViewChild } from '@angular/core';
import { ChangeImage } from 'src/app/use-classes/ChangeImage';
import { NestedService } from 'src/app/services/nested.service';
import { PetType } from 'src/app/modules/game-classes/petType';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { delay } from 'rxjs/operators';
import { BounsType } from 'src/app/modules/game-classes/bonusType';

@Component({
  selector: 'app-game-shop',
  templateUrl: './game-shop.component.html',
  styleUrls: ['./game-shop.component.scss']
})
export class GameShopComponent implements OnInit {

  constructor(public nestedService: NestedService, public modalService: NgbModal) { }

  eat: string;
  backgroundOnTop: string = 'assets/backgrounds/background0.png';
  changeImageController = new ChangeImage();
  formatedPetsArray: Array<any> = new Array();
  modalReference: NgbModalRef;
  interval: any;
  closeResult: string;
  petsArray: Array<PetType>;
  playerPetsArray: Array<any> = new Array();
  shopItemsArray: Array<any> = new Array();
  bonusesArray: Array<BounsType> = new Array();
  playerPoints: number;
  playerAssets: Array<any> = new Array();
  activePet: any;
  usedPet: any;

  ngOnInit() {
    this.backgroundShow();
    this.getPlayerAssets();
    this.getAllPets();
  }


  // add new values to objects in bonusesArray
  initBonuses(): void {
    this.bonusesArray.forEach(el => {
      el.cost = el.bonusLevel === 1 ? 100 : (el.bonusLevel * 200);
      el.bonusName === 'Health' ? el.bonusDescription = 'Increase tryes per round' : el.bonusName === 'Coin'
        ? el.bonusDescription = 'Multiply coins per round' : el.bonusName === 'Gravity'
          ? el.bonusDescription = 'Change your gravity' : el.bonusDescription = '';

      if (this.usedPet === 'frog')
        this.eat = 'fly';
      if (this.usedPet === 'horse')
        this.eat = 'carrot';
      if (this.usedPet === 'pig')
        this.eat = 'rye';
    });

  }

  //  Change background
  backgroundShow(): void {
    let i = 0;
    this.interval = setInterval(() => {
      if (i === this.changeImageController.actualImage.length) {
        i = 1;
      }
      this.backgroundOnTop = this.changeImageController.setImage(i++);
    }, 2000)
  }

  // get all player data from rest
  getPlayerAssets(): void {
    this.nestedService.gameService.getPlayerAssets()
      .subscribe(data => {
        this.playerAssets = data;
        this.playerPetsArray = data.petsArray;
        this.usedPet = data.petType;
        this.playerPoints = data.score;
        this.bonusesArray = data.bonusesArray;
        this.initBonuses();
      })
  }

  // get all pets from rest
  getAllPets(): void {
    this.nestedService.gameService.getAllPets()
      .pipe(
        delay(700)
      )
      .subscribe(data => {
        this.petsArray = data;
        this.makeShop();
      });
  }

  // create pets shop
  makeShop(): void {
    for (let i = 0; i < this.petsArray.length; i++) {
      for (let j = 0; j < this.playerPetsArray.length; j++) {
        if (this.petsArray[i]._id === this.playerPetsArray[j].petId) {
          this.formatedPetsArray.push({
            _id: this.petsArray[i]._id,
            name: this.petsArray[i].name,
            cost: this.petsArray[i].cost,
            active: true,
            usedPet: this.usedPet === this.petsArray[i].name ? true : false
          });
        }
      }
    }

    for (let i = 0; i < this.petsArray.length; i++) {
      let isCopy = false;
      for (let j = 0; j < this.formatedPetsArray.length; j++) {
        if (this.petsArray[i]._id === this.formatedPetsArray[j]._id) {
          isCopy = true;
          break;
        } else {
          isCopy = false;
        }
      }
      if (!isCopy) {
        this.formatedPetsArray.push({
          _id: this.petsArray[i]._id,
          name: this.petsArray[i].name,
          cost: this.petsArray[i].cost,
          active: false,
          usedPet: this.usedPet === this.petsArray[i].name ? true : false
        });
      }
    }
  }
  //  buy pet 
  buyPet(): void {
    if (this.playerPoints < this.activePet.cost) {
      this.nestedService.gameService.showSnackBar('You dont have enaugh points', 'CLOSE');
      this.modalService.dismissAll();
      return;
    }
    this.nestedService.gameService.buyPet(this.activePet._id, this.activePet.name, this.activePet.cost)
      .subscribe(data => {
        console.log(data);
        this.playerPoints -= this.activePet.cost;
        this.activePet.active = true;
      });
    this.modalService.dismissAll();
  }

  // modal actions
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // Open modal window to buy pet
  open(content, pet) {
    this.activePet = pet;
    if (!this.activePet.active) {
      this.modalReference = this.modalService.open(content, { centered: true });
      console.log(this.activePet);
      this.modalReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  //  set active pet
  setPlayerPet(pet) {
    this.formatedPetsArray.forEach(element => {
      element.usedPet = false;
    });

    if (pet.name === 'frog')
      this.eat = 'fly';
    if (pet.name === 'horse')
      this.eat = 'carrot';
    if (pet.name === 'pig')
      this.eat = 'rye';

    this.nestedService.gameService.setActivePet(pet.name)
      .subscribe(data => {
        console.log(data);
      })
    pet.usedPet = !pet.usedPet;

  }

  //  update bonus level
  increaseLevel(bonus: BounsType): void {
    if (this.playerPoints >= bonus.cost && bonus.bonusLevel < 5) {

      if (bonus.bonusLevel === 1) {
        bonus.cost = bonus.cost;
      } else {
        bonus.cost = bonus.bonusLevel * 200;
      }

      bonus.bonusLevel++;
      this.playerPoints -= bonus.cost;
      this.nestedService.gameService.bonusUpdate(bonus.bonusName, bonus.bonusLevel, bonus.cost)
        .subscribe(data => {
          console.log(data);
          this.initBonuses();
        })

    } else {
      this.nestedService.gameService.showSnackBar('You dont have enaugh points', 'CLOSE');
    }
  }
}
