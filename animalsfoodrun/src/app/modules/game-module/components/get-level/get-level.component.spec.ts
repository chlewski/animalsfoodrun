import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetLevelComponent } from './get-level.component';

describe('GetLevelComponent', () => {
  let component: GetLevelComponent;
  let fixture: ComponentFixture<GetLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
