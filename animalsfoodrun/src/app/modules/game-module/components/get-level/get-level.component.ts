import { Component, OnInit, OnDestroy } from '@angular/core';
import { NestedService } from 'src/app/services/nested.service';
import { Round } from 'src/app/modules/game-classes/round';
import { Router } from '@angular/router';
import { ChangeImage } from 'src/app/use-classes/ChangeImage';

@Component({
  selector: 'app-get-level',
  templateUrl: './get-level.component.html',
  styleUrls: ['./get-level.component.scss']
})
export class GetLevelComponent implements OnInit, OnDestroy {

  constructor(public nestedService: NestedService, private router: Router) { }

  playerData: any;
  allRoundsArray: Array<Round>;
  stars: any;
  formatedRoundsArray: Array<any> = new Array();
  backgroundOnTop: string = 'assets/backgrounds/background0.png';
  changeImageController = new ChangeImage();
  interval: any;

  ngOnInit() {
    this.backgroundShow();
    this.getAllRounds();
  }

  //  Make array of rounds from rest
  getAllRounds(): void {
    this.nestedService.gameService.getAllRounds()
      .subscribe(data => {
        this.allRoundsArray = data;
        this.nestedService.gameService.getPlayerAssets()
          .subscribe(playerData => {
            this.playerData = playerData.roundsFinished;
            this.filterData();
          });
        console.log(this.formatedRoundsArray)
      });
  }
  //filter data about player and rounds and create array with specific data
  filterData(): void {
    console.log(this.playerData, this.allRoundsArray);

    for (let i = 0; i < this.allRoundsArray.length; i++) {
      for (let j = 0; j < this.playerData.length; j++) {
        if (this.allRoundsArray[i]._id === this.playerData[j].roundId) {
          this.formatedRoundsArray.push({
            _id: this.playerData[j].roundId,
            stars: this.playerData[j].stars,
            roundNumber: this.allRoundsArray[i].roundNumber,
            score: this.playerData[j].score
          })
        }
      }
    }
    for (let i = 0; i < this.allRoundsArray.length; i++) {
      let isCopy = false;
      for (let j = 0; j < this.formatedRoundsArray.length; j++) {
        if (this.allRoundsArray[i]._id === this.formatedRoundsArray[j]._id) {
          isCopy = true;
          break;
        } else {
          isCopy = false;
        }
      }
      if (!isCopy) {
        this.formatedRoundsArray.push({
          _id: this.allRoundsArray[i]._id,
          stars: 0,
          roundNumber: this.allRoundsArray[i].roundNumber,
          score: 0,
          notActive: true
        });
      }
    }
  }

  //  Save round number in game service
  saveRoundNumber(round: any): any {
    //console.log(round);
    const roundIndex = this.formatedRoundsArray.findIndex((r) => r === round);
    console.log(round)
    if (round !== this.formatedRoundsArray[0]) {
      if (this.formatedRoundsArray[roundIndex - 1].notActive) {
        this.nestedService.gameService.showSnackBar(`Finish round ${roundIndex}`, 'CLOSE')
        return;
      }
    }

    this.router.navigate([`game/play/${round._id}`]);
  }

  //Change background
  backgroundShow() {
    let i = 1;
    this.interval = setInterval(() => {
      if (i === this.changeImageController.actualImage.length) {
        i = 0;
      }
      this.backgroundOnTop = this.changeImageController.setImage(i++);
      console.log('0interval')
    }, 4000)
  }

  //  Byway for ngFor
  counter(i: number): Array<any> {
    return new Array(i);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

}
