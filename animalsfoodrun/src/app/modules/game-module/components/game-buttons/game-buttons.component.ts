import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GameButtons } from 'src/app/modules/game-classes/gameButtons';
import { GameTypes } from 'src/app/modules/game-classes/gameTypes';
import { NestedService } from 'src/app/services/nested.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-game-buttons',
  templateUrl: './game-buttons.component.html',
  styleUrls: ['./game-buttons.component.scss']
})

export class GameButtonsComponent implements OnInit {
  @Input() buttons: GameButtons;
  @Output() buttonsReq: EventEmitter<any> = new EventEmitter();

  stars: number;
  currentRoundId: any
  constructor(private nestedService: NestedService, private route: Router, private router: ActivatedRoute) {
    //  after click next round reload path
    this.route.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.getIdFromRoute();
    this.setButtons();
    console.log('roundNumber', this.buttons.roundNumber);
  }
  getIdFromRoute(): void {
    this.router.params.subscribe(params => {
      this.currentRoundId = params['id'];
      console.log("currentId: ", this.currentRoundId);
    });
  }
  // count stars 
  countStars(): void {

    if (this.buttons.score * 100 / (this.buttons.maxScore * this.buttons.coinsMultiplier) === 100) {
      console.log(this.buttons.score * 100 / (this.buttons.maxScore * this.buttons.coinsMultiplier));
      this.stars = 3;
    } else if (this.buttons.score * 100 / (this.buttons.maxScore * this.buttons.coinsMultiplier) < 100
      && this.buttons.score * 100 / this.buttons.maxScore * this.buttons.coinsMultiplier > 50) {
      console.log('stars 2');
      this.stars = 2;
    } else {
      this.stars = 1;
    }
  }

  // set buttons depends on Game communicate type 
  setButtons() {
    if (this.buttons.buttonsType === GameTypes.WIN) {
      this.countStars();
      this.nestedService.gameService.scoreSend(this.buttons.score, this.buttons.maxScore, this.buttons.roundId, this.stars)
        .subscribe(
          (data)=>{
            this.nestedService.gameService.addScoreToRoundArray(this.buttons.score,this.buttons.roundId).subscribe();
          },
          err => console.log(err),
          ()=>{
            console.log("obs finished")
          }
        )
        
      this.buttons.stars = this.stars;
    } else if (this.buttons.buttonsType === GameTypes.END_GAME) {
      this.buttons.stars = 0;
    }
  }

  //  change to next round
  nextRound() {
    this.nestedService.gameService.getAllRounds()
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          if (data[i].roundNumber === this.buttons.roundNumber && data.length !== this.buttons.roundNumber) {
            this.route.navigate([`/game/play/${data[i + 1]._id}`]);
          }
        }
      });
  }

  useExtraLife() {
    let emitData = {
      used: true,
      hearts: this.buttons.hearts - 1
    }
    this.buttonsReq.emit(emitData);
    console.log('used');
  }
  // reload page if user click repeat round
  reloadPage() {
    this.route.navigate(['/game/choose-round'])
    .then(()=>{this.route.navigate(['game/play/', this.currentRoundId])})
  }

  //  Byway for ngFor
  counter(i: number): Array<any> {
    return new Array(i);
  }


}
