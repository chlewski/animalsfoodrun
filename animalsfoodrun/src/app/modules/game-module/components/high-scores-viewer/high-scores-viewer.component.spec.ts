import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighScoresViewerComponent } from './high-scores-viewer.component';

describe('HighScoresViewerComponent', () => {
  let component: HighScoresViewerComponent;
  let fixture: ComponentFixture<HighScoresViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighScoresViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighScoresViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
