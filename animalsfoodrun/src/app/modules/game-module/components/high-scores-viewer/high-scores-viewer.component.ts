import { Component, OnInit, Input, ElementRef, SimpleChanges, SimpleChange, Output, EventEmitter, ViewChild } from '@angular/core';
import { NestedService } from 'src/app/services/nested.service';
import { $ } from 'protractor';

@Component({
  selector: 'app-high-scores-viewer',
  templateUrl: './high-scores-viewer.component.html',
  styleUrls: ['./high-scores-viewer.component.scss']
})
export class HighScoresViewerComponent implements OnInit {
  @Input() roundId: string;
  @Input() roundNumber: string;
  @Input() roundsArray: Array<any>;
  @Output() back: EventEmitter<any> = new EventEmitter();
  @Output() next: EventEmitter<any> = new EventEmitter();
  @Output() closeViewer: EventEmitter<string> = new EventEmitter();

  @ViewChild('scores', { static: true }) scores;

  constructor(public nestedService: NestedService) { }
  highestScores: Array<any>;
  ngOnInit() {

  }

  ngOnChanges(changes: Map<string, SimpleChange>): void {
    console.log(changes);
    this.getHighestScores();
  }

  getHighestScores(): void {
    this.nestedService.gameService.getHighestScores(this.roundId)
      .subscribe(data => {
        this.highestScores = data;
        console.log(data);
      })
  }

  changePage(status: string, event?) {

    if (status === 'next' || event.type === 'swipeleft') {
      this.next.emit({ roundId: this.roundId, roundNumber: this.roundNumber });
    } else if (status === 'back' || event.type === 'swiperight') {
      this.back.emit({ roundId: this.roundId, roundNumber: this.roundNumber });
    } else if (status === 'close') {
      this.closeViewer.emit();
    }

  }



  ngOnDestroy(): void {
    this.highestScores = [];
  }

}
