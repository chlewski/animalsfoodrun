import { Component, OnInit, ElementRef } from '@angular/core';
import { ChangeImage } from 'src/app/use-classes/ChangeImage';
import { NestedService } from 'src/app/services/nested.service';
import {style, animate, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-top-scorers',
  templateUrl: './top-scorers.component.html',
  styleUrls: ['./top-scorers.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [ 
        style({opacity:0}),
        animate(500, style({opacity:1})) 
      ]),
      transition(':leave', [ 
        animate(500, style({opacity:0})) 
      ])
    ])
  ]
})
export class TopScorersComponent implements OnInit {

  constructor(public nestedService: NestedService) { }
  backgroundOnTop: string = 'assets/backgrounds/background0.png';
  changeImageController = new ChangeImage();
  interval: any;
  roundsArray: Array<any>;
  showViewer: boolean;
  roundId: string;
  roundNumber: number;


  ngOnInit() {
    this.backgroundShow();
    this.getAllRounds();
  }
  getAllRounds(): void {
    this.nestedService.gameService.getAllRounds()
      .subscribe(data => {
        this.roundsArray = data;
      });
  }

  //Change background
  backgroundShow() {
    let i = 1;
    this.interval = setInterval(() => {
      if (i === this.changeImageController.actualImage.length) {
        i = 1;
      }
      console.log('interval');
      this.backgroundOnTop = this.changeImageController.setImage(i++);
    }, 3000)
  }

  viewerOn(id: string, roundNumber) {
    this.roundId = id;
    this.roundNumber = roundNumber
    if (this.showViewer) {
      this.showViewer = false;
    }
    this.showViewer = true;
  }

  previousRound(event) {
    let roundIndex = this.roundsArray.findIndex((x) => { return x._id === event.roundId });
    this.roundsArray[roundIndex].active = false;
    if (roundIndex === 0) {
      this.roundsArray[this.roundsArray.length - 1].active = true;
      this.roundId = this.roundsArray[this.roundsArray.length - 1]._id;
      this.roundNumber = this.roundsArray[this.roundsArray.length - 1].roundNumber;
    } else {
      this.roundsArray[roundIndex - 1].active = true;
      this.roundId = this.roundsArray[roundIndex - 1]._id;
      this.roundNumber = this.roundsArray[roundIndex - 1].roundNumber;
    }
  }

  nextRound(event) {
    let roundIndex = this.roundsArray.findIndex((x) => { return x._id === event.roundId });
    this.roundsArray[roundIndex].active = false;
    if (roundIndex >= this.roundsArray.length - 1) {
      this.roundsArray[0].active = true;
      this.roundId = this.roundsArray[0]._id;
      this.roundNumber = this.roundsArray[0].roundNumber;
    } else {
      this.roundsArray[roundIndex + 1].active = true;
      this.roundId = this.roundsArray[roundIndex + 1]._id;
      this.roundNumber = this.roundsArray[roundIndex + 1].roundNumber;
    }
  }
  closeViewer() {
    this.showViewer = false;
    this.roundsArray.forEach(el => {
      el.active = false;
    })
  }
  changeActiveRound(round) {
    this.roundsArray.forEach(el =>{
      el.active = false;
    });
    if (typeof round.active === undefined) {
      console.log('undefinedclick')
      round.active = true;
    } else if (round.active) {
      console.log('trueclick')
      return;
    } else {
      console.log('falseclick')      
      round.active = !round.active;
    }
  }
}