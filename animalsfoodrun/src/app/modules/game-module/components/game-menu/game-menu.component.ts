import { Component, OnInit, OnDestroy } from '@angular/core';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { ChangeImage } from 'src/app/use-classes/ChangeImage';
import { Router } from '@angular/router';
import { NestedService } from 'src/app/services/nested.service';

@Component({
  selector: 'app-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.scss']
})
export class GameMenuComponent implements OnInit, OnDestroy {
  
  constructor(private previousRouteService: PreviousRouteService, private nestedService: NestedService, private router: Router) { }
  backgroundOnTop: string = 'assets/backgrounds/background0.png';
  changeImageController = new ChangeImage();
  interval :any;
  
  ngOnInit() {
    this.checkLastRoute();
    this.backgroundShow();    
  }
  
  checkLastRoute() {
    if (this.previousRouteService.getPreviousUrl().includes('/game/play')) {
      window.onpopstate = function (e) { window.history.forward(); }
    }
  }
  
  //Change background
  backgroundShow() {
    let i = 1;
    this.interval = setInterval(() => {
      if (i === this.changeImageController.actualImage.length) {
        i = 1;
      }
      console.log('interval');
      this.backgroundOnTop = this.changeImageController.setImage(i++);
    }, 3000)
  }
  
  //logout user
  logOut(){
    this.nestedService.authService.logOut();
    this.router.navigate(['/signin']);
  }
  ngOnDestroy(): void {
    clearInterval(this.interval);
  }
  
}
