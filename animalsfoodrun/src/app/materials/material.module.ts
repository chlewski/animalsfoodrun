import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const material = [
  MatSnackBarModule
]
@NgModule({
  declarations: [],
  imports: [
    material
  ]
})
export class MaterialModule { }
