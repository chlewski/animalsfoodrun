import { MatSnackBar } from '@angular/material/snack-bar';

export class SnackBar {
    constructor(private snackBar: MatSnackBar){}
    showSnackBar(message: string, option: string) {
       return  this.snackBar.open(message, option, { duration: 2000 });
    }
}