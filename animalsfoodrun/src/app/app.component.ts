import { Component, ViewChild, ElementRef } from '@angular/core';
import { NestedService } from './services/nested.service';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { merge, of, fromEvent, Observable } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { Router, ActivationEnd } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'animalsfoodrun';
  audio = new Audio('assets/background-music.mp3');
  file: MediaObject
  musicStatus: boolean;
  networkState$: Observable<boolean>;
  musicButtonShow: boolean = true;
  constructor(public nestedService: NestedService, public media: Media, public router: Router) {
    this.playAudio();
    this.checkConnection();
    this.musicButton();
  }
  checkConnection() {

    this.networkState$ = merge(
      of(navigator.onLine),
      fromEvent(window, 'online').pipe(mapTo(true)),
      fromEvent(window, 'offline').pipe(mapTo(false))
    );
    console.log(this.networkState$);
  }
  playAudio() {

    if (typeof window['cordova'] !== 'undefined') {
      console.log('cordova music')
      let file = new Media();

      if (localStorage.getItem('Music') === 'ON') {
        file.create('assets/background-music.mp3').play();
        this.musicStatus = true;
      }
      else {
        file.create('assets/background-music.mp3').pause();
        this.musicStatus = false;
      }
    } else {

      if (localStorage.getItem('Music') === 'ON') {
        this.audio.play();
        this.musicStatus = true;
      }
      else {
        this.audio.pause();
        this.musicStatus = false;
      }
    }
  }

  playButton() {
    this.nestedService.gameService.musicPlay();
    this.playAudio();
  }
  musicButton() {
    this.router.events.subscribe((url: any) => {
      if (url instanceof ActivationEnd) {
        if (this.router.url.includes('game/play/')) {
          console.log('uuuu');
          this.musicButtonShow = false;
        } else {
          this.musicButtonShow = true;
        }
      }
    });;

  }

}
