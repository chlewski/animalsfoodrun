import { Injectable } from '@angular/core';
import { CanActivate, Router, CanActivateChild, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NestedService } from '../services/nested.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private nestedService: NestedService, private router: Router) { }

  canActivate() {
    if (this.nestedService.authService.getToken()) {
      return true;
    }
    this.router.navigate(['/signin'])
    return false;
  }

  
}