import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NestedService } from '../services/nested.service';

@Injectable({
  providedIn: 'root'
})
export class AuthBackGuard implements CanActivate {

  constructor(private router: Router, private nestedService: NestedService) { }

  canActivate(): boolean {

    if (!this.nestedService.authService.getToken()) {
      return true;
    }
    this.router.navigate(['/game/menu']);
    return false;
  }
}