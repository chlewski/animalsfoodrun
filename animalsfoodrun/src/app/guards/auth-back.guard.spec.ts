import { TestBed, async, inject } from '@angular/core/testing';

import { AuthBackGuard } from './auth-back.guard';

describe('AuthBackGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthBackGuard]
    });
  });

  it('should ...', inject([AuthBackGuard], (guard: AuthBackGuard) => {
    expect(guard).toBeTruthy();
  }));
});
