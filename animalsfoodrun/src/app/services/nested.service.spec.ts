import { TestBed } from '@angular/core/testing';

import { NestedService } from './nested.service';

describe('NestedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NestedService = TestBed.get(NestedService);
    expect(service).toBeTruthy();
  });
});
