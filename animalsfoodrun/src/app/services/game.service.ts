import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_URL } from '../urls/Url';
import { Round } from '../modules/game-classes/round';
import { PetType } from '../modules/game-classes/petType';
import { Router } from '@angular/router';
import { SnackBar } from '../tools/snack-bar';
import { MatSnackBar } from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient, private router: Router, private snackBar: MatSnackBar) { }
  public activeRoundNumber: number;
  public snackBarInstance = new SnackBar(this.snackBar);

  // get all rounds from REST
  getAllRounds(): Observable<Array<any>> {
    return this.http.get<Array<Round>>(`${SERVER_URL}game/get-all-rounds`);
  }

  // get single round from REST
  getSingleRound(id: string): Observable<Round> {
    return this.http.get<Round>(`${SERVER_URL}game/get-single-round/${id}`);
  }

  //  get player things
  getPlayerAssets(): Observable<any> {
    return this.http.get<any>(`${SERVER_URL}game/get-player-details/`);
  }

  //  send score to REST
  scoreSend(score: number, maxScore: number, roundId: string, stars: number): Observable<any> {
    return this.http.post<any>(`${SERVER_URL}game/score-update`, { score: score, maxScore: maxScore, roundId: roundId, stars: stars });
  }

  //  send score to REST
  addScoreToRoundArray(score: number, roundId: string): Observable<any> {
    return this.http.post<any>(`${SERVER_URL}game/add-score`, { roundId: roundId, score: score });
  }

  //  get all pets from rest
  getAllPets(): Observable<Array<PetType>> {
    return this.http.get<Array<PetType>>(`${SERVER_URL}game/get-pets`);
  }

  //  add pet to player when player buy pet
  buyPet(petId: string, petName: string, cost: number): Observable<any> {
    return this.http.post<any>(`${SERVER_URL}game/add-pet-user`, { petId: petId, petName: petName, petCost: cost });
  }

  //  set active player on rest side
  setActivePet(petName: string): Observable<any> {
    return this.http.post<any>(`${SERVER_URL}game/set-pet-user`, { petName: petName });
  }

  //  get highest scores
  getHighestScores(roundId: string): Observable<Array<any>> {    
    return this.http.get<Array<any>>(`${SERVER_URL}game/get-high-scores/${roundId}`);
  }

  //  global function for back menu button
  goToMenu() {
    this.router.navigate(['/game/menu']);
  }

  //  update player bonus rest
  bonusUpdate(bonusName: string, bonusLevel: number, bonusCost: number): Observable<any> {
    return this.http.post<any>(`${SERVER_URL}game/bonus-update`, { bonusName: bonusName, bonusLevel: bonusLevel, bonusCost: bonusCost });
  }
  //  global snackbar
  showSnackBar(message: string, option: string): void {
    this.snackBarInstance.showSnackBar(message, option);
  }

  //  Music play
  musicPlay() {
    if (localStorage.getItem('Music') === 'OFF')
      localStorage.setItem('Music', 'ON');
    else
      localStorage.setItem('Music', 'OFF')
  }

}
