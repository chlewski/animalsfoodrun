import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import { GameService } from './game.service';

@Injectable({
  providedIn: 'root'
})
export class NestedService {
  constructor(public authService: AuthService, public gameService: GameService) { }
}
