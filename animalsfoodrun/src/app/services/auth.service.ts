import { Injectable } from '@angular/core';
import { SERVER_URL } from '../urls/Url'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  getToken(): string {
    return localStorage.getItem('token');
  }

  logIn(username: string, password: string): Observable<any> {
    return this.http.post<User>(`${SERVER_URL}auth/signin`, { username: username, password: password });
  }
  singUp(username: string, password: string): Observable<any> {
    return this.http.post<User>(`${SERVER_URL}auth/signup`, { username: username, password: password })
  }

  logOut() {
    localStorage.removeItem('token');
  }

}