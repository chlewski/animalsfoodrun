import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthBackGuard } from './guards/auth-back.guard';



const routes: Routes = [
  { path: '', redirectTo: '/signin', pathMatch: 'full' },
  { path: 'signin', canActivate: [AuthBackGuard], component: LoginComponent },
  { path: 'signup', canActivate: [AuthBackGuard], component: RegisterComponent },  
  { path: 'game', canActivate: [AuthGuard], loadChildren: () => import('./modules/game-module/game.module').then(module => module.GameModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }