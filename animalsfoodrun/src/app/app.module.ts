import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NestedService } from './services/nested.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { PreviousRouteService } from './services/previous-route.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './materials/material.module';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import * as Hammer from 'hammerjs';



export class HammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'swipe': { direction: Hammer.DIRECTION_ALL }
  }
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,    
    RegisterComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MaterialModule,
    IonicModule.forRoot({
      backButtonText: 'Go Back'
    }),
  ],
  providers: [NestedService, PreviousRouteService, MaterialModule, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true,
  }, {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig
    }, Media],
  bootstrap: [AppComponent]
})
export class AppModule { }
