export class ChangeImage {
    actualImage: Array<string>;

    constructor() {
        this.actualImage = new Array();
        for (let i = 0; i <= 6; i++) {
            this.actualImage.push(`assets/backgrounds/background${i}.png`);
        }
    }
    setImage(imageNumber: number): string {
        return this.actualImage[imageNumber];
    }

}